import os
from ftplib import FTP_TLS
import ftplib

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from products_spider.settings import FTP_CONFIG, ENVIRONMENT

from products_spider.spiders.boconcept_spider import BoConceptSpider


class BoConceptIntegration:
    def __init__(self):
        self.settings = get_project_settings()
        self.ftp_config = FTP_CONFIG
        self.env = ENVIRONMENT

    def _configure(self):
        if self.env == 'TEST':
            self.ftp_config = {
                'DESTINATION_FTP_SERVER': 'pure-ftpd',
                'DESTINATION_FTP_PORT':'21',
                'DESTINATION_FTP_USER': 'username',
                'DESTINATION_FTP_SECRET': 'mypass',
                'DESTINATION_FTP_PATH': '/home/username/',
            }

    def extract(self):
        self._configure()
        process = CrawlerProcess(self.settings)
        process.crawl(BoConceptSpider)
        process.start()

    def _get_files(self):
        files = list()
        paths = [self.settings['IMAGES_STORE'], self.settings['FILES_STORE']]
        for path in paths:
            for filename in os.listdir(path):
                files.append([filename, os.path.join(path, filename)])

        return files

    def send_to_ftp(self):
        ftp = self._get_ftp_server()
        for filename, fullpath in self._get_files():
            with open(fullpath, 'rb') as file:
                ftp.storbinary(f'STOR {filename}', file)
        
        ftp.quit()

    def _get_ftp_server(self):
        if self.env == 'TEST':
            return ftplib.FTP(
                self.ftp_config['DESTINATION_FTP_SERVER'],
                self.ftp_config['DESTINATION_FTP_USER'],
                self.ftp_config['DESTINATION_FTP_SECRET'],
            )
        
        ftp = FTP_TLS()
        ftp.debugging = 2
        ftp.connect(self.ftp_config['DESTINATION_FTP_SERVER'], 21)
        ftp.login(
            self.ftp_config['DESTINATION_FTP_USER'], 
            self.ftp_config['DESTINATION_FTP_SECRET']
        )

        return ftp

    def execute(self):
        self.extract()
        self.send_to_ftp()

integration = BoConceptIntegration()
integration.execute()
