import csv
import scrapy
import stringcase

from products_spider.items import ProductsSpiderItem


class BoConceptSpider(scrapy.Spider):
    name = 'boconcept'
    allowed_domains = [
        'www.boconcept.com',
    ]
    base_url = 'https://www.boconcept.com'
    custom_settings = {
        'FEED_FORMAT': 'csv',
        'FEED_URI': 'test.csv'
    }

    attributes_map = {
        'Composición:': 'materials_composition',
        'Cordón:': 'materials_cordon',
        'Color:': 'materials_color',
        'Revestimiento trasero:': 'materials_rear_lining',
        'Lámpara:': 'materials_lamp',
        'Altura:': 'dimensions_height',
        'Anchura:': 'dimensions_width',
        'Profundidad:': 'dimensions_depth',
        'Altura del asiento:': 'dimensions_seat_height',
        'Altura del reposabrazos:': 'dimensions_armrest_height',
        'Altura de las patas:': 'dimensions_leg_height',
        'Carga máxima:': 'dimensions_maximum_load',
        'Longitud (extendida):': 'dimensions_extended_length',
        'Altura (extendida):': 'dimensions_extended_height',
        'Anchura (extendida):': 'dimensions_extended_width',
        'Longitud del cable:': 'dimensions_cable_length',
        'Diámetro:': 'dimensions_diameter',
        'Tamaño de la pantalla:': 'dimensions_screen_size',
        'Plazas:': 'dimensions_places',
        'Peso:': 'dimensions_weight',
    }

    def start_requests(self):
        search_url = 'https://www.boconcept.com/es-co/search?q={}'

        with open('products.csv', mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                yield scrapy.Request(
                    url=search_url.format(row['ean']),
                    callback=self.parse_search_product,
                    cb_kwargs={'ean': row},
                )

    def _get_product_attributes(self, response, id='') -> dict:
        selector_path = '//div[@id="{}"]/div'.format(id)
        elements = response.xpath(selector_path)

        attributes = {}

        for element in elements:
            attribute_name = element.xpath('span/text()').get()
            attribute_name = self.attributes_map.get(attribute_name)
            attribute_value = element.xpath('text()').getall()
            if attribute_name:
                attributes[attribute_name] = ' '.join(attribute_value).strip()

        return attributes


    def _get_product_files(self, response, id='', ean='') -> dict:
        selector_path = '//div[@id="{}"]/ul/li'.format(id)
        elements = response.xpath(selector_path)
        files = {}
        if elements:
            for element in elements:
                file_url = element.xpath('a/@href').get()
                if element.xpath('a/@rel').get() == 'nofollow':
                    continue
                file_type = element.xpath('a/text()').get()
                file_name = '{} - {}'.format(file_type, ean)
                files[file_type] = {
                    'file_url': file_url,
                    'file_name': file_name,
                }
        return files

    def _strip_string(self, string):
        if string:
            return string.strip()
        return None

    def parse_product_detail(self, response, **kwargs):
        ean = kwargs.get('ean')
        title = response.xpath('//div[@class="page-title"]/span[@class="product-name page-name"]/text()').get()
        title = self._strip_string(title)

        product_description = response.xpath('//span[@class="product-description"]/text()').get()

        if not product_description:
            product_description = response.xpath('//h1[@class="short-description"]/text()').get()

        product_description = self._strip_string(product_description)

        price = response.xpath('//span[@class="price-sales"]/text()').getall()[1]
        price = self._strip_string(price)

        images = response.xpath('//div[@class="swiper-container"]/ul/li/a/@href').getall()

        product_data = {
            'name': title,
            'description': product_description,
            'price': price,
            'files': self._get_product_files(response, 'product-instructions', ean['ean']),
            'image_urls': ['{}{}'.format(self.base_url, image) for image in images]
        }

        product_data.update(ean)
        product_data.update(self._get_product_attributes(response, 'Materials'))
        product_data.update(self._get_product_attributes(response, 'Dimensions'))

        yield ProductsSpiderItem(**product_data)

    def parse_search_product(self, response, **kwargs):
        ean = kwargs.get('ean')
        product_detail_url = response.xpath('//meta[@property="og:url"]/@content').get()

        if product_detail_url:
            yield response.follow(
                product_detail_url,
                callback=self.parse_product_detail,
                cb_kwargs={'ean': ean}
            )