# -*- coding: utf-8 -*-

# Scrapy settings for products_spider project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

import os


BOT_NAME = 'products_spider'

SPIDER_MODULES = ['products_spider.spiders']
NEWSPIDER_MODULE = 'products_spider.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; ' \
             '+http://www.google.com/bot.html) Chrome/W.X.Y.Z‡ Safari/537.36 '

# Obey robots.txt rules
ROBOTSTXT_OBEY = False
COOKIES_DEBUG = False
HTTPCACHE_ENABLED = False

# Enable Proxy
# PROXY_POOL_ENABLED = True

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
# DOWNLOAD_DELAY = 5
# The download delay setting will honor only one of:
# CONCURRENT_REQUESTS_PER_DOMAIN = 32
# CONCURRENT_REQUESTS_PER_IP = 32

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'products_spider.middlewares.ProductScrapingSpiderMiddleware': 543,
#}

ITEM_PIPELINES = {
    'products_spider.pipelines.SaveProductPipeLine': 4,
    'products_spider.pipelines.BoConceptImagePipeLine': 2,
    'products_spider.pipelines.BoConceptFilePipeLine': 3,
}

IMAGES_STORE = '/opt/images/' 
FILES_STORE =  '/opt/files/'

ENVIRONMENT = 'TEST' # You can switch between TEST and PRODUCTION

# Change this values only for PRODUCTION environment
DATABASE = {
    'drivername': 'postgres',
    'host': '',
    'port': '5432',
    'username': '',
    'password': '',
    'database': 'scraper',
}

FTP_CONFIG = {
    'DESTINATION_FTP_SERVER': '',
    'DESTINATION_FTP_PORT':'',
    'DESTINATION_FTP_USER': '',
    'DESTINATION_FTP_SECRET': '',
    'DESTINATION_FTP_PATH': '',
}


# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
# For Proxy
# DOWNLOADER_MIDDLEWARES = {
#    # ...
#    'scrapy_proxy_pool.middlewares.ProxyPoolMiddleware': 610,
#    'scrapy_proxy_pool.middlewares.BanDetectionMiddleware': 620,
#    # ...
# }

# DOWNLOADER_MIDDLEWARES = {
#     'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
#     'scrapy_user_agents.middlewares.RandomUserAgentMiddleware': 400,
# }

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
AUTOTHROTTLE_ENABLED = True
# The initial download delay
AUTOTHROTTLE_START_DELAY = 1
# The maximum download delay to be set in case of high latencies
AUTOTHROTTLE_MAX_DELAY = 10
# The average number of requests Scrapy should be sending in parallel to
# each remote server
AUTOTHROTTLE_TARGET_CONCURRENCY = 1.5
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'