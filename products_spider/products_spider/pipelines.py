# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


from datetime import datetime

import scrapy
from itemadapter import ItemAdapter
from scrapy.exceptions import DropItem
from scrapy.pipelines.files import FilesPipeline
from scrapy.pipelines.images import ImagesPipeline
from sqlalchemy.orm import sessionmaker

from products_spider.models import Product, create_table, db_connect


class ProductsSpiderPipeline:
    def process_item(self, item, spider):
        return item


class BoConceptFilePipeLine(FilesPipeline):
    def get_media_requests(self, item, info):
        for key in item['files'].keys():
            print('FILE URL: {}'.format(item['files'][key]['file_url']))
            yield scrapy.Request(
                item['files'][key]['file_url'],
                meta={'filename': item['files'][key]['file_name']}
            )

    def file_path(self, request, response=None, info=None):
        filename = request.meta["filename"]
        return filename


class BoConceptImagePipeLine(ImagesPipeline):
    def get_media_requests(self, item, info):
        counter = 1
        for image_url in item['image_urls']:
            filename = '{}-{}.jpg'.format(item['ean'], counter)
            counter += 1
            yield scrapy.Request(image_url, meta={'filename': filename})

    def file_path(self, request, response=None, info=None):
        filename = request.meta["filename"]
        return filename


class SaveProductPipeLine(object):
    def __init__(self):
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        session = self.Session()
        product_item = item
        del product_item['files']
        product = Product(**item)

        exist_product = session.query(Product).filter_by(ean=product.ean).first()
        if exist_product:
            exist_product.description = item['description']
            exist_product.price = item['price']
            exist_product.materials_composition = item['materials_composition']
            exist_product.updated_at = datetime.now()
            product = exist_product
        else:
            product.created_at = datetime.now()
            product.updated_at = datetime.now()

        try:
            session.add(product)
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()

        return item            
