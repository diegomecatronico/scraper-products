# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ProductsSpiderItem(scrapy.Item):
    ean = scrapy.Field()
    description = scrapy.Field()
    price = scrapy.Field()
    name = scrapy.Field()
    image_urls = scrapy.Field()
    files = scrapy.Field()
    materials_composition = scrapy.Field()
    materials_cordon = scrapy.Field()
    materials_lamp = scrapy.Field()
    materials_rear_lining = scrapy.Field()
    materials_color = scrapy.Field()
    dimensions_height = scrapy.Field()
    dimensions_width = scrapy.Field()
    dimensions_depth = scrapy.Field()
    dimensions_seat_height = scrapy.Field()
    dimensions_armrest_height = scrapy.Field()
    dimensions_leg_height = scrapy.Field()
    dimensions_maximum_load = scrapy.Field()
    dimensions_extended_length = scrapy.Field()
    dimensions_cable_length = scrapy.Field()
    dimensions_diameter = scrapy.Field()
    dimensions_screen_size = scrapy.Field()
    dimensions_places = scrapy.Field()
    dimensions_extended_height = scrapy.Field()
    dimensions_extended_width = scrapy.Field()
    dimensions_weight = scrapy.Field()