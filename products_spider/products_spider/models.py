from sqlalchemy import Column, Integer, String, create_engine, TIMESTAMP
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base

from products_spider import settings

DeclarativeBase = declarative_base()

def db_connect():
    if settings.ENVIRONMENT == 'TEST':
        return create_engine(URL(**{
            'drivername': 'postgres',
            'host': 'db',
            'username': 'postgres',
            'password': 'postgres',
            'database': 'postgres',
        }))
    return create_engine(URL(**settings.DATABASE))

def create_table(engine):
    DeclarativeBase.metadata.create_all(engine)


class Product(DeclarativeBase):
    __tablename__ = 'bc_products'

    id = Column(Integer, primary_key=True)
    name = Column('name', String)
    ean = Column('ean', String)
    description = Column('description', String, nullable=True)
    price = Column('price', String)
    materials_composition = Column('materials_composition', String, nullable=True)
    materials_cordon = Column('materials_cordon', String, nullable=True)
    materials_lamp = Column('materials_lamp', String, nullable=True)
    materials_rear_lining = Column('materials_rear_lining', String, nullable=True)
    materials_color = Column('materials_color', String, nullable=True)
    dimensions_height = Column('dimensions_height', String, nullable=True)
    dimensions_width = Column('dimensions_width', String, nullable=True)
    dimensions_depth = Column('dimensions_depth', String, nullable=True)
    dimensions_seat_height = Column('dimensions_seat_height', String, nullable=True)
    dimensions_armrest_height = Column('dimensions_armrest_height', String, nullable=True)
    dimensions_leg_height = Column('dimensions_leg_height', String, nullable=True)
    dimensions_maximum_load = Column('dimensions_maximum_load', String, nullable=True)
    dimensions_extended_length = Column('dimensions_extended_length', String, nullable=True)
    dimensions_cable_length = Column('dimensions_cable_length', String, nullable=True)
    dimensions_diameter = Column('dimensions_diameter', String, nullable=True)
    dimensions_screen_size = Column('dimensions_screen_size', String, nullable=True)
    dimensions_places = Column('dimensions_places', String, nullable=True)
    dimensions_extended_height = Column('dimensions_extended_height', String, nullable=True)
    dimensions_extended_width = Column('dimensions_extended_width', String, nullable=True)
    dimensions_weight = Column('dimensions_weight', String, nullable=True)
    updated_at = Column('updated_at', TIMESTAMP)
    created_at = Column('created_at', TIMESTAMP)
    image_urls = Column('image_urls', String)