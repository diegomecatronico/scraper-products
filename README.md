# Product scraper
The present project extracts the product data from BoConcept CO site given a SKU product list. The scrapped data fields are:
- Product Name
- Product description
- SKU, barcodes.
- Product images from main gallery.
- Dimensions and materials attributes.

For achieve this, the project involves these frameworks and components:
- Scrapy for scraping the data from BoConcept site. [Scrapy documentation](https://docs.scrapy.org/en/latest/index.html)
- SQLAlchemy for persist the obtained data in a PostgreSQL database. [Scrapy documentation](https://docs.scrapy.org/en/latest/index.html)

- Docker to facilitate the deployment process https://docs.docker.com/



## Previous steps

For implement this project in your production environment, you will need install Docker and Docker Compose. You can find the installation instructions in the official site of Docker. Please make sure about following the instructions of your current Linux distro of your environment.

Additionally you will need to setup your FTP credentials in order to save the obtained images and files, and your Database credentials for saving the data.  For that  edit the file settings.py and find the variables 

    DATABASE = {
	    'drivername': 'postgres',
	    'host': '',
	    'port': '',
	    'username': '',
	    'password': '',
	    'database': 'scraper',
	}
	FTP_CONFIG = {
	    'DESTINATION_FTP_SERVER': '',
	    'DESTINATION_FTP_PORT':'',
	    'DESTINATION_FTP_USER': '',
	    'DESTINATION_FTP_SECRET': '',
	    'DESTINATION_FTP_PATH': '',
	}



## Building and executing the scraper
Once finished the previous step you can build the Docker image using this command:

    docker-compose build

Then execute it:

    docker-compose up


